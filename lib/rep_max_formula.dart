import 'dart:math';

enum RepMaxFormulae {
  wathan,
  brzycki,
  epley,
  mcglothin,
  lobmardi,
  mayhew,
  oconner,
  wendley
}

class RepMaxPair {
  int reps;
  double weight;

  RepMaxPair(this.reps, this.weight);
}

abstract class RepMaxFormula {
  /// Calculates a one rep maximum from the set performed in [maxRepSet].
  double _calculate(RepMaxPair maxRepSet);

  /// Calculates an n-rep maximum from a [oneRepMax].
  double _invert(double oneRepMax, int desiredRepRange);

  /// Calculate the estimated maximum weight for [reps].
  ///
  /// Given a [RepMaxPair] which corresponds to a users [maxRepSet],
  /// find the estimated weight a user can perform for [reps].
  double calculate(RepMaxPair maxRepSet, int reps) {
    double oneRepMax = _calculate(maxRepSet);

    if (reps == 1) {
      return oneRepMax.roundToDouble();
    } else {
      return _invert(oneRepMax, reps).roundToDouble();
    }
  }
}

class Wathans extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return (100 * maxRepSet.weight) /
        (48.8 + 53.8 * exp(-0.075 * maxRepSet.reps));
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return (oneRepMax * (48.8 + 53.8 * exp(-0.075 * desiredRepRange))) / 100;
  }
}

class Bryzycki extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return maxRepSet.weight * (36 / (37 - maxRepSet.reps));
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return (oneRepMax * (37 - desiredRepRange)) / 36;
  }
}

class Epley extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return maxRepSet.weight * (1 + (maxRepSet.reps));
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return oneRepMax / (1 + desiredRepRange / 30);
  }
}

class McGlothin extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return (100 * maxRepSet.weight) / (101.3 - 2.67123 * maxRepSet.reps);
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return (oneRepMax * (101.3 - 2.67123 * desiredRepRange)) / 100;
  }
}

class Lombardi extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return maxRepSet.weight * pow(maxRepSet.reps, 0.1);
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return oneRepMax / (pow(desiredRepRange, 0.1));
  }
}

class Mayhew extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return (100 * maxRepSet.weight) /
        (52.2 + 41.9 * exp(-0.055 * maxRepSet.reps));
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return (oneRepMax * (52.2 + 41.9 * exp(-0.055 * desiredRepRange))) / 100;
  }
}

class OConner extends RepMaxFormula {
  @override
  double _calculate(RepMaxPair maxRepSet) {
    return maxRepSet.weight * (1 + maxRepSet.reps / 40);
  }

  @override
  double _invert(double oneRepMax, int desiredRepRange) {
    return oneRepMax / (1 + (desiredRepRange / 40));
  }
}
