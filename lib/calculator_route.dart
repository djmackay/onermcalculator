import 'package:flutter/material.dart';

import 'rep_max_formula.dart';
import 'calculator.dart';

class CalculatorRoute extends StatefulWidget {
  const CalculatorRoute({super.key});

  @override
  State<StatefulWidget> createState() {
    return CalculatorRouteState();
  }
}

class RepMaxDataTable extends StatelessWidget {
  final List<RepMaxPair> repMaxes;
  final String unit;

  const RepMaxDataTable({super.key, required this.repMaxes, required this.unit});

  DataRow _buildRow(RepMaxPair repMaxPair) {
     return DataRow(
      cells: <DataCell>[
        DataCell(Text(repMaxPair.reps.toString())),
        DataCell(Text(repMaxPair.weight.toString())),
      ],
    );

  }

  @override
  Widget build(BuildContext context) {
    return DataTable(
      columns: <DataColumn>[
        const DataColumn(
          label: Expanded(
            child: Text(
              'Reps',
              style: TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
        DataColumn(
          label: Expanded(
            child: Text(
              'Weight ($unit)',
              style: const TextStyle(fontStyle: FontStyle.italic),
            ),
          ),
        ),
      ],
      rows: repMaxes.map<DataRow>(_buildRow).toList()
    );
  }
}

class CalculatorRouteState extends State<CalculatorRoute> {
  final RepMaxCalculator _calc = RepMaxCalculator();

  final _weightController = TextEditingController();
  final _repController = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  WeightUnit? _unit = WeightUnit.kilogram;
  String _unitString = "kg";

  List<RepMaxPair> _repMaxes = [];

  Widget _buildWeightInput() {
    return Flexible(
      child: TextFormField(
        decoration: InputDecoration(labelText: "Weight ($_unitString)"),
        keyboardType: TextInputType.number,
        controller: _weightController,
        validator: (value) {
          if (value == null || value == "") {
            return "Please type a value";
          }
          if (double.tryParse(value) == null) {
            return "Please type a number";
          }
          if (double.parse(value) < 0) {
            return "Please type a positive number";
          }
          return null;
        },
      ),
    );
  }

  Widget _buildRepInput() {
    return Flexible(
      child: TextFormField(
        decoration: const InputDecoration(labelText: "Reps"),
        keyboardType: TextInputType.number,
        controller: _repController,
        validator: (value) {
          if (value == null || value == "") {
            return "Please type a value";
          }
          if (double.tryParse(value) == null) {
            return "Please type a number";
          }
          if (double.parse(value) < 0) {
            return "Please type a positive number";
          }
          if (double.parse(value) > 10) {
            return "Please type a rep value less then 10";
          }
          return null;
        },
      ),
    );
  }

  Widget _buildRepMaxesTable() {
    if (_repMaxes.isEmpty) {
      return const Icon(
          Icons.fitness_center_rounded,
          color: Color(0xFF4BBEE3),
          size: 48.0,
      );
    }

    return RepMaxDataTable(repMaxes: _repMaxes, unit: _unitString);
  }

  Widget _buildInputs() {
    return Container(
      padding: const EdgeInsets.all(16.0),
      child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  _buildWeightInput(),
                  _buildRepInput(),
                ]),
            ElevatedButton(
              onPressed: () {
                if (_formKey.currentState!.validate()) {
                  double weight = double.parse(_weightController.text);
                  int reps = int.parse(_repController.text);
                  setState(() {
                    _repMaxes = _calc.calculateRepMaxes(weight, reps);
                  });
                }
                // Hide the keyboard when the user presses 'calculate'.
                FocusScope.of(context).requestFocus(FocusNode());
              },
              // splashColor: const Color(0x00f6511d),
              child: const Text(
                'Calculate',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _handleWeightUnitChange(WeightUnit? value) {
    setState(() {
      _unit = value;

      switch (_unit) {
        case WeightUnit.kilogram:
          _unitString = "kg";
          break;
        case WeightUnit.pound:
          _unitString = "lb";
          break;
        case null:
          // TODO: Handle this case.
      }
    });
  }

  Widget _buildWeightUnitSelector() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        const Text(
          'Unit',
          style: TextStyle(
            fontSize: 16.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Radio(
              value: WeightUnit.kilogram,
              groupValue: _unit,
              onChanged: _handleWeightUnitChange,
            ),
            const Text(
              'Kilograms (kg)',
              style: TextStyle(fontSize: 16.0),
            ),
            Radio(
              value: WeightUnit.pound,
              groupValue: _unit,
              onChanged: _handleWeightUnitChange,
            ),
            const Text(
              'Pounds (lb)',
              style: TextStyle(
                fontSize: 16.0,
              ),
            ),
          ],
        )
      ],
    );
  }

  Widget _buildDrawer() {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          const SizedBox(
            height: 88,
            child: DrawerHeader(
              decoration: BoxDecoration(
                color: Color(0xFF4BBEE3),
              ),
              child: Text(
                'Settings',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
          ),
          _buildWeightUnitSelector(),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        drawer: _buildDrawer(),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            mainAxisSize: MainAxisSize.max,
            children: <Widget>[
              AppBar(
                title: const Text(
                  "1RM Calculator",
                  style: TextStyle(color: Colors.white),
                ),
                backgroundColor: const Color(0xFF4BBEE3),
              ),
              Expanded(
                child: SizedBox(
                  width: double.infinity,
                  child: _buildRepMaxesTable(),
                ),
              ),
              _buildInputs(),
            ],
          ),
        ));
  }
}
