import 'rep_max_formula.dart';

enum WeightUnit { kilogram, pound }

class RepMaxCalculator {
  RepMaxFormula _formula = Wathans(); // Default..

  RepMaxCalculator();

  double calculate(double weight, int reps) {
    if (reps > 10) {
      // TODO: Alert user reps less than 10.
    }
    if (reps < 1) {
      // TODO: Also alert.
    }

    return _formula.calculate(RepMaxPair(reps, weight), 1);
  }

  List<RepMaxPair> calculateRepMaxes(double weight, int reps) {
    RepMaxPair maxRepSet = RepMaxPair(reps, weight);

    List<RepMaxPair> repMaxes = [];
    List<int> repRanges = [1, 2, 3, 4, 5, 6, 8, 10];
    for (int repRange in repRanges) {
      double estimatedWeight = _formula.calculate(maxRepSet, repRange);
      var repMaxPair = RepMaxPair(repRange, estimatedWeight);
      repMaxes.add(repMaxPair);
    }

    return repMaxes;
  }

  _setFormulae(RepMaxFormulae formula) {
    switch (formula) {
      case RepMaxFormulae.wathan:
        _formula = Wathans();
        break;
      case RepMaxFormulae.brzycki:
        _formula = Bryzycki();
        break;
      case RepMaxFormulae.epley:
        _formula = Epley();
        break;
      case RepMaxFormulae.wendley:
        _formula = Epley(); // Wendley is just eply restated.
        break;
      case RepMaxFormulae.mcglothin:
        _formula = McGlothin();
        break;
      case RepMaxFormulae.lobmardi:
        _formula = Lombardi();
        break;
      case RepMaxFormulae.mayhew:
        _formula = Mayhew();
        break;
      case RepMaxFormulae.oconner:
        _formula = OConner();
        break;
    }
  }
}
